from collections import defaultdict
import csv
from operator import itemgetter
import os
import re
import sys


def main(considered_cities):
    if len(considered_cities) is 0:
        considered_cities = {'Kraków', 'Katowice'}
    DATA_DIR = 'data'
    CITY_LOOKUP_FILE = '{}/Kody_stacji_pomiarowych.csv'.format(DATA_DIR)

    city_keys = None
    with open(CITY_LOOKUP_FILE) as cities_file:
        file_reader = csv.reader(cities_file)
        next(file_reader)
        city_keys = {row[3]: row[5] for row in file_reader if row[5] in considered_cities}

    data_files = ["{}/{}".format(DATA_DIR, data_file.name)
                  for data_file
                  in os.scandir(DATA_DIR)
                  if data_file.is_file() and re.match("2015_.*\.csv", data_file.name)
                  ]

    smog_sum = defaultdict(int)

    for input_file_name in data_files:
        with open(input_file_name) as input_file:
            file_reader = csv.reader(input_file)
            header = next(file_reader)
            for _ in range(2):
                next(file_reader)
            indices = [i for i, station in enumerate(header) if station in city_keys]
            for row in file_reader:
                for index in indices:
                    try:
                        smog_sum[city_keys[header[index]]] += float(row[index])
                    except ValueError:
                        pass

    cities_stats = [(smog, city) for (city, smog) in smog_sum.items()]
    for i, (smog, city) in enumerate(sorted(cities_stats, key=itemgetter(0), reverse=True)):
        print("{}. {}\t({})".format(i + 1, city, int(smog)))


if __name__ == '__main__':
    considered_cities = {city for city in sys.argv[1:]}
    main(considered_cities)
